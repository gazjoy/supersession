$(document).ready(function(){
	$(function() {
		var pull 		= $('#toggle');
			menu 		= $('nav ul');
			menuHeight	= menu.height();
	
		$(pull).on('click', function(e) {
			e.preventDefault();
			menu.slideToggle();
			$(this).toggleClass('open');
		});
		
		$('.show').bxSlider({
        auto:	'true',
        mode: 'fade'
  	});
  
	});
});